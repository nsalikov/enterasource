# -*- coding: utf-8 -*-
import scrapy


class PartsSpider(scrapy.Spider):
    name = 'parts'
    allowed_domains = ['enterasource.com']
    start_urls = ['https://www.enterasource.com/components?limit=25']


    def parse(self, response):
        items_css = 'h2.product-name a ::attr(href)'
        items = response.css(items_css)

        if not items:
            return

        for item in items:
            yield response.follow(item, callback=self.parse_item)

        pages_css = '.pages ol li.next a.next ::attr(href)'
        pages = response.css(pages_css).extract()

        for page in pages:
            yield response.follow(page, callback=self.parse)


    def parse_item(self, response):
        d = {}

        d['id'] = response.css('input[name="product"] ::attr(value)').extract_first()
        d['name'] = response.css('.product-name h1::text').extract_first()
        d['url'] = response.url
        d['breadcrumbs'] = dict(zip(response.css('.breadcrumbs ul li a span::text').extract(), response.css('.breadcrumbs ul li a::attr(href)').extract()))
        d['condition'] = response.css('.condition span.value::text').extract_first()
        d['specs'] = dict(zip(response.css('#product-attribute-specs-table th.label::text').extract(), response.css('#product-attribute-specs-table td.data::text').extract()))

        yield d
